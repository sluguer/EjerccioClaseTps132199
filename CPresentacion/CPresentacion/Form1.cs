﻿using Clogica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CPresentacion
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close(); 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Usuarios oUsuarios = new Usuarios();

            oUsuarios.insertarUsuario(Int64.Parse(txtIdUsuario.Text ), txtNombre.Text , txtApellido.Text ,
                txtNickName.Text , txtClave.Text , txtEmail.Text );

        }
    }
}
