﻿using CDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clogica
{
    public class Usuarios
    {
        private Int64 idUsuario;
        private string nombre;
        private string apellido;
        private string nickName;
        private string clave; 
        private string email;

        public long IdUsuario
        {
            get
            {
                return idUsuario;
            }

            set
            {
                idUsuario = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Apellido
        {
            get
            {
                return apellido;
            }

            set
            {
                apellido = value;
            }
        }

        public string NickName
        {
            get
            {
                return nickName;
            }

            set
            {
                nickName = value;
            }
        }

        public string Clave
        {
            get
            {
                return clave;
            }

            set
            {
                clave = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        public void  insertarUsuario(Int64 idUsuario, string nombre, string apellido, string nickName, string clave, string email)
        {
            Conexion oConx = new Conexion();

            string sentencia;
            sentencia = " exec InsertarUsuarios " + idUsuario + ",'" + nombre + "','" + apellido + "','" + nickName + "','" + clave + "','" + email + "'";
            oConx.ejecutar_sentencias_sql(sentencia);

        }

    }
}
