﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TallerDeProgramacion
{
    class Program
    {
        static void Main(string[] args)
        {
            double pesoMercancia, valorMercancia, valorTarifa, valorPromocion, valorDescuento;
            string esLunes, tipoPago;
            PedirDatos(out pesoMercancia, out valorMercancia, out esLunes, out tipoPago);
            valorTarifa = CalcularTarifa(pesoMercancia);
            valorDescuento = CalculaDescuento(valorMercancia, valorTarifa);
            valorPromocion = CalcularPromocion(esLunes, tipoPago, valorMercancia, valorTarifa);
            MostrarResultados(valorTarifa, valorDescuento, valorPromocion);
        }


        //////////////

         static void PedirDatos(out double pesoMercancia, out double valorMercancia, out string esLunes, out string tipoPago)
        {
            Console.WriteLine("Peso Mercancias :");
            pesoMercancia = double.Parse(Console.ReadLine());
            Console.WriteLine("Valor Mercancias :");
            valorMercancia = double.Parse(Console.ReadLine());
            Console.WriteLine("Es Lunes:  [S]i, [N]o ");
            esLunes = Console.ReadLine();
            Console.WriteLine("Tipo de Pago :  [E]fectivo [T]arjeta");
            tipoPago = Console.ReadLine();

        }
        /////////////
         static double CalcularTarifa(double pesoMercancia)
        {

            if (pesoMercancia < 100)
            {
                return 20000;
            }
            if (pesoMercancia <= 150)
            {
                return 25000;
            }
            if (pesoMercancia <= 200)
            {
                return 30000;
            }

            return 35000 + (pesoMercancia - 200) / 10 * 2000;
        }
        //////////////
         static double CalculaDescuento(double valorMercancia, double valorTarifa)
        {
            if (valorMercancia < 300000)
            {
                return 0;
            }
            if (valorMercancia <= 600000)
            {
                return valorTarifa * 0.1;
            }
            if (valorMercancia <= 1000000)
            {
                return valorTarifa * 0.2;
            }

            return valorTarifa * 0.3;

        }
         static double CalcularPromocion(string esLunes, string tipoPago, double valorMercancia, double valorTarifa)
        {
            if (esLunes == "S" && tipoPago == "T")
            {
                return valorTarifa * 0.5;
            }
            if (tipoPago == "E" && valorMercancia > 1000000)
            {
                return valorTarifa * 0.4;
            }
            return 0;
        }
         static  void MostrarResultados(double valorTarifa, double valorDescuento, double valorPromocion)
        {
            double totalAPagar;
            Console.WriteLine("Tarifa: $" +valorTarifa);
            if (valorDescuento > valorPromocion)
            {
                Console.WriteLine("Descuento: $"+ valorDescuento);
                totalAPagar = valorTarifa - valorDescuento;
            }
            else
            {
                Console.WriteLine("Promocion: $"+ valorPromocion);
                totalAPagar = valorTarifa - valorPromocion;

            }

            Console.WriteLine("Total a Pagar: $" +totalAPagar);
            Console.ReadKey();
        }


    }
}
